package com.getmatic.firsttask;

import com.sun.istack.internal.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FirstTask {
    private List<List<String>> readyCombinations = new ArrayList<>();

    public void start() {
        Scanner scanner = new Scanner(System.in);
        String target = readTarget(scanner);
        List<String> dictionary = readDictionary(scanner);
        scanner.close();

        Iterator<String> dictionaryIterator = dictionary.iterator();
        while (dictionaryIterator.hasNext()) {
            if (!target.contains(dictionaryIterator.next())) dictionaryIterator.remove();
        }
        
        findCombinations1(null, target, dictionary);
        printReadyCombinations(this.readyCombinations);

        this.readyCombinations.clear();

        findCombinations2(target, dictionary);
        printReadyCombinations(this.readyCombinations);
    }

    private void findCombinations1(@Nullable List<String> combination, String target, List<String> dictionary) {
        for (String dictionaryItem : dictionary) {
            if (target.startsWith(getCombinationAsString(combination).concat(dictionaryItem))) {
                List<String> possibleCombination = new ArrayList<>();
                if (combination != null) possibleCombination.addAll(combination);
                possibleCombination.add(dictionaryItem);

                if (target.equals(getCombinationAsString(possibleCombination)))
                    this.readyCombinations.add(possibleCombination);
                else
                    findCombinations1(possibleCombination, target, dictionary);
            }
        }
    }

    private void findCombinations2(String target, List<String> dictionary) {
        LinkedList<List<String>> possibleCombinations = new LinkedList<>();
        possibleCombinations.add(Collections.singletonList(""));

        while (!possibleCombinations.isEmpty()) {
            List<String> possibleCombination = possibleCombinations.get(0);

            for (String dictionaryItem : dictionary) {
                if (target.startsWith(getCombinationAsString(possibleCombination).concat(dictionaryItem))) {
                    List<String> combination = new ArrayList<>(possibleCombination);
                    combination.add(dictionaryItem);

                    if (target.equals(getCombinationAsString(combination))) {
                        combination.remove("");
                        this.readyCombinations.add(combination);
                    } else {
                        possibleCombinations.add(combination);
                    }
                }
            }

            possibleCombinations.removeFirst();
        }
    }

    private String getCombinationAsString(@Nullable List<String> combination) {
        String flatCombination = "";

        if (combination != null) {
            for (int i = 0; i < combination.size(); i++) {
                flatCombination += combination.get(i);
            }
        }

        return flatCombination;
    }

    private String readTarget(Scanner scanner) {
        System.out.println("Enter target string: ");
        return scanner.next();
    }

    private List<String> readDictionary(Scanner scanner) {
        System.out.println("Enter dictionary strings separated with comma: ");
        scanner.nextLine();
        String[] dictionaryArray = scanner.nextLine().split(",");

        for (int i = 0; i < dictionaryArray.length; i++) {
            dictionaryArray[i] = dictionaryArray[i].trim();
        }

        return Arrays.asList(dictionaryArray);
    }

    private void printReadyCombinations(List<List<String>> combinations) {
        System.out.println();
        System.out.println("Possible combinations:");
        for (List<String> combination : combinations) {
            for (int i = 0; i < combination.size(); i++) {
                System.out.print(combination.get(i).concat(" "));
            }

            System.out.println();
        }
    }
}