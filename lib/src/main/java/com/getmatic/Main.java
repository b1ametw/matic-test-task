package com.getmatic;


import com.getmatic.firsttask.FirstTask;
import com.getmatic.secondtask.models.Checkout;
import com.getmatic.secondtask.models.discount.FreeGumForJuiceAndStrawberryDiscount;
import com.getmatic.secondtask.models.discount.FruitTeaDiscount;
import com.getmatic.secondtask.models.discount.StrawberriesDiscount;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please select the task by entering it's number (1 or 2):");

        switch (scanner.next()) {
            case "1":
                startFirstTask();
                break;
            case "2":
                startSecondTask();
                break;

            default:
                System.out.println("Task not found!");
        }

        scanner.close();
    }

    private static void startFirstTask() {
        FirstTask firstTask = new FirstTask();
        firstTask.start();
    }

    private static void startSecondTask() {
        Checkout checkout1 = Checkout.newCheckout(new FruitTeaDiscount(), new StrawberriesDiscount(), new FreeGumForJuiceAndStrawberryDiscount());
        checkout1.scan("FR");
        checkout1.scan("SR");
        checkout1.scan("FR");
        checkout1.scan("FR");
        checkout1.scan("CF");
        System.out.println("Scanned products: FR, SR, FR, FR, CF");
        System.out.println("Total is: " + checkout1.getTotalPrice());
        System.out.println();

        Checkout checkout2 = Checkout.newCheckout(new FruitTeaDiscount(), new StrawberriesDiscount(), new FreeGumForJuiceAndStrawberryDiscount());
        checkout2.scan("FR");
        checkout2.scan("FR");
        System.out.println("Scanned products: FR, FR");
        System.out.println("Total is: " + checkout2.getTotalPrice());
        System.out.println();

        Checkout checkout3 = Checkout.newCheckout(new FruitTeaDiscount(), new StrawberriesDiscount(), new FreeGumForJuiceAndStrawberryDiscount());
        checkout3.scan("SR");
        checkout3.scan("SR");
        checkout3.scan("FR");
        checkout3.scan("SR");
        System.out.println("Scanned products: SR, SR, FR, SR");
        System.out.println("Total is: " + checkout3.getTotalPrice());
        System.out.println();

        Checkout checkout4 = Checkout.newCheckout(new FruitTeaDiscount(), new StrawberriesDiscount(), new FreeGumForJuiceAndStrawberryDiscount());
        checkout4.scan("BG");
        checkout4.scan("BG");
        checkout4.scan("SR");
        checkout4.scan("SR");
        checkout4.scan("AJ");
        System.out.println("Scanned products: BG, BG, SR, SR, AJ");
        System.out.println("Total is: " + checkout4.getTotalPrice());
        System.out.println();
    }
}
