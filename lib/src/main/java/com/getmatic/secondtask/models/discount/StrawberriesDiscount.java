package com.getmatic.secondtask.models.discount;

import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.SKU;

import java.util.List;

public class StrawberriesDiscount extends Discount {
    @Override
    public void apply(List<Product> products) {
        List<Product> productsWithDiscount = getProductsBySKU(SKU.SR, products);

        for (Product product : productsWithDiscount) {
            product.setDiscountedPrice(4.5);
        }
    }

    protected boolean isApplicable(List<Product> products) {
        return getProductsBySKU(SKU.SR, products).size() >= 3;
    }
}
