package com.getmatic.secondtask.models.product;

import java.util.Objects;

public class Product {
    private SKU sku;
    private String name;
    private double regularPrice;
    private double discountedPrice;

    public SKU getSKU() {
        return this.sku;
    }

    public String getName() {
        return this.name;
    }

    Product() {

    }

    public double getRegularPrice() {
        return this.regularPrice;
    }

    public double geеPriceWithDiscount() {
        return discountedPrice;
    }

    public void setRegularPrice(double regularPrice) {
        this.regularPrice = regularPrice;
    }

    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public boolean isDiscountApplied() {
        return this.regularPrice != this.discountedPrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSKU(SKU sku) {
        this.sku = sku;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSKU(), getRegularPrice());
    }

    @Override
    public boolean equals(Object obj) {
        return (obj != null) && (obj instanceof Product) && (((Product) obj).sku.equals(this.sku)) && (((Product) obj).regularPrice == this.regularPrice);
    }
}
