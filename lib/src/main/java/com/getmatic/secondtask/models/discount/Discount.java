package com.getmatic.secondtask.models.discount;

import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.SKU;

import java.util.ArrayList;
import java.util.List;

public abstract class Discount {
    private Discount next;

    public void applyDiscount(List<Product> productList) {
        if (isApplicable(productList)) {
            apply(productList);
        }

        if (this.next != null) {
            this.next.applyDiscount(productList);
        }
    }

    public Discount setNext(Discount next) {
        this.next = next;
        return next;
    }

    protected abstract void apply(List<Product> products);

    protected abstract boolean isApplicable(List<Product> products);

    protected static List<Product> getProductsBySKU(SKU sku, List<Product> allProducts) {
        List<Product> products = new ArrayList<>();

        for (Product product : allProducts)
            if (product.getSKU().equals(sku)) products.add(product);

        return products;
    }
}
