package com.getmatic.secondtask.models.discount;

import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.SKU;

import java.util.List;

public class FreeGumForJuiceAndStrawberryDiscount extends Discount {
    @Override
    protected void apply(List<Product> products) {
        List<Product> juiceList = getProductsBySKU(SKU.AJ, products);
        List<Product> strawberryList = getProductsBySKU(SKU.SR, products);
        List<Product> gumList = getProductsBySKU(SKU.BG, products);

        int availableFreeGums = 0;

        for (int i = 0; i < Math.min(juiceList.size(), strawberryList.size()); i++)
            availableFreeGums++;

        for (int i = 0; i < gumList.size(); i++) {
            if (availableFreeGums > 0) {
                gumList.get(i).setDiscountedPrice(0);
                availableFreeGums--;
            }
        }
    }

    @Override
    protected boolean isApplicable(List<Product> products) {
        List<Product> juiceList = getProductsBySKU(SKU.AJ, products);
        List<Product> strawberryList = getProductsBySKU(SKU.SR, products);

        return Math.min(juiceList.size(), strawberryList.size()) > 0;
    }
}
