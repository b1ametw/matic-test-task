package com.getmatic.secondtask.models;

import com.getmatic.secondtask.models.discount.Discount;
import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.ProductsFabric;

import java.util.ArrayList;
import java.util.List;

public class Checkout {
    private List<Product> productList = new ArrayList<>();
    private Discount firstDiscount;

    public static Checkout newCheckout(Discount... discounts) {
        Checkout checkout = new Checkout();

        if (discounts.length > 0) checkout.firstDiscount = discounts[0];

        for (int i = 0; i < discounts.length - 1; i++)
            discounts[i].setNext(discounts[i + 1]);

        return checkout;
    }

    private Checkout() {
    }

    public void scan(String productShortName) {
        Product product = ProductsFabric.getProduct(productShortName);
        this.productList.add(product);
    }

    private void applyDiscounts() {
        if (this.firstDiscount != null) this.firstDiscount.applyDiscount(this.productList);
    }

    public double getTotalPrice() {
        applyDiscounts();

        double totalPrice = 0;

        for (Product product : this.productList)
            totalPrice += product.geеPriceWithDiscount();

        return totalPrice;
    }
}
