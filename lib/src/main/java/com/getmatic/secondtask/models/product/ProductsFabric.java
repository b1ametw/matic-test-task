package com.getmatic.secondtask.models.product;


public class ProductsFabric {
    public static Product getProduct(String productShortName) {
        SKU sku = getSkuForShortName(productShortName);
        if (sku == null)
            throw new RuntimeException(String.format("No such product: %s!", productShortName));

        Product product = new Product();
        product.setSKU(sku);
        product.setName(sku.getName());

        switch (sku) {
            case FR:
                product.setRegularPrice(3.11);
                product.setDiscountedPrice(3.11);
                break;

            case SR:
                product.setRegularPrice(5.0);
                product.setDiscountedPrice(5.0);
                break;

            case CF:
                product.setRegularPrice(11.23);
                product.setDiscountedPrice(11.23);
                break;

            case AJ:
                product.setRegularPrice(7.25);
                product.setDiscountedPrice(7.25);
                break;

            case BG:
                product.setRegularPrice(0.5);
                product.setDiscountedPrice(0.5);
                break;
        }

        return product;
    }

    private static SKU getSkuForShortName(String productShortName) {
        for (SKU sku : SKU.values()) {
            if (sku.name().equalsIgnoreCase(productShortName)) return sku;
        }

        return null;
    }
}
