package com.getmatic.secondtask.models.discount;

import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.SKU;

import java.util.List;

public class FruitTeaDiscount extends Discount {
    @Override
    public void apply(List<Product> products) {
        List<Product> teaList = getProductsBySKU(SKU.FR, products);

        for (int i = 0; i < teaList.size(); i++) {
            if (i % 2 != 0) {
                teaList.get(i).setDiscountedPrice(0);
            }
        }
    }

    protected boolean isApplicable(List<Product> products) {
        return getProductsBySKU(SKU.FR, products).size() >= 2;
    }
}
