package com.getmatic.secondtask.models.product;

public enum SKU {
    FR("FRUIT_TEA"),
    SR("STRAWBERRIES"),
    CF("COFFEE"),
    AJ("APPLE_JUICE"),
    BG("BUBBLE GUM");

    private String name;
    private double price;

    SKU(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name();
    }
}
