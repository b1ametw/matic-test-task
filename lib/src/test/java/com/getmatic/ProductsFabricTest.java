package com.getmatic;


import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.ProductsFabric;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProductsFabricTest {

    @Test
    public void newProductRegularAndDiscountedPricesShouldBeEqual() {
        Product product = ProductsFabric.getProduct("FR");
        assertEquals("Price should be equal", product.getRegularPrice() == product.geеPriceWithDiscount(), true);
    }

    @Test(expected = RuntimeException.class)
    public void unknownProductShouldThrowException() {
        Product product = ProductsFabric.getProduct("FRR");
    }
}
