package com.getmatic;


import com.getmatic.secondtask.models.product.Product;
import com.getmatic.secondtask.models.product.ProductsFabric;

import org.junit.Assert;
import org.junit.Test;

public class ProductTest {

    @Test
    public void productShouldBeMarkedDiscounted() {
        Product product = ProductsFabric.getProduct("FR");
        product.setRegularPrice(2);
        product.setDiscountedPrice(1);

        Assert.assertEquals(product.isDiscountApplied(), true);
    }
}
